const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


// user registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: reqBody.password,
		mobileNo: reqBody.mobileNo,

		password: bcrypt.hashSync(reqBody.password, 10)
	})
	// check if email is already used
	return User.find({email: reqBody.email}).then(result =>{
		if(result.length > 0){
			return {error: "Email already used"};
		}
		else{
			return newUser.save().then((user, error) =>{
				if (error) {
					return false;
				}
				else{
					return {message: "Registration successful"};
				};
			});
		};
	});	
};



// user login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result === null){
			return {
				login: "Failed",
				message: "Email not in user database"
			};
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {
					login: "Successful",
					access: auth.createAccessToken(result)
				};
			}
			else{
				return {
					login: "Failed",
					message: "Incorrect password"
				};
			};
		};
	});
};


// get user profile
module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody).then((result,error) => {
		if(error){
			console.log(error);
			return false;
		}
		else {
			result.password = "";
			return result;	
		};
	});
};

// (ADMIN function) set as Admin
module.exports.setAsAdmin = (reqBody) => {
	// console.log(reqBody);
	return User.findById(reqBody.userId).then((result,error) => {
		if(error){
			console.log(error);
			return false;
		}
		else {
			if(result.isAdmin){
				return {error: "User already an Admin"};
			}
			else{
				result.isAdmin = true;
				return result.save().then((user, error) =>{
					if (error) {
						return false;
					}
					else{
						user.password = "";
						return {message: "Update successful", user};
					};
				});
			};
		};
	});
};

// (ADMIN function) remove as Admin
module.exports.removeAdmin = (reqBody) => {
	// console.log(reqBody);
	return User.findById(reqBody.userId).then((result,error) => {
		if(error){
			console.log(error);
			return false;
		}
		else {
			if(result.isAdmin === false){
				return {error: "User not an Admin"};
			}
			else{
				result.isAdmin = false;
				return result.save().then((user, error) =>{
					if (error) {
						return false;
					}
					else{
						user.password = "";
						return {message: "Update successful", user};
					};
				});
			};
		};
	});
};

// (ADMIN function) get all users
module.exports.getAllUsers = () =>{
	return User.find({},'firstName lastName email isAdmin').then(result =>{
		return result;
	});
};
