const Order = require("../models/Order.js");
const Product = require("../models/Product.js");
const User = require("../models/User.js");


// create order
module.exports.createOrder = async (data) =>{
	let total;
	let isAvailable = await Product.findById(data.productId).then((product,error) =>{
		if(error){
				return false;
		}
		else{
			if(product.stock < 1){
				return false;
			}
			else{
				product.stock -= 1;
				total = product.price;
				return product.save().then((result,error) =>{
					if (error){
						return false;
					}
					else{
						return true;
					};
				});
			}
			
		};
	});

	if(isAvailable){
		let isUserUpdated = await User.findById(data.userId).then((user,error) => {
			if(error){
					return false;
			}
			else{
				let newOrder = new Order({
					userId: data.userId,
					productId: data.productId,
					totalAmount: total,
				});

				return newOrder.save().then((order, error) =>{
					if (error) {
						return false;
					}
					else{
						user.orders.push({orderId: order.id});
						return user.save().then((user,error) =>{
							if (error) {
								return false;
							}
							else {
								console.log(order);
								console.log(user);
								return true;
							};
						})
					};
				});	
			};
			
			});

		if(isUserUpdated){
			return {"Checkout": "Successful"};
		}
		else{
			return false;
		};
	}
	else{
		return {"Error": "Item out of stock"}
	}
};

// (ADMIN function) get allOrders 

module.exports.getAllOrders = () =>{
	return Order.find({}).then(result =>{
		return result;
	});
};


// (USER function) get myOrders
module.exports.myOrders = (userId) =>{
	return Order.find({userId: userId}).then((result) =>{
		return result;
	});
};
